package main

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/mitchellh/go-homedir"
	"github.com/upamune/go-esa/esa"
	"github.com/urfave/cli"
	"io"
	"os"
	"regexp"
	"strconv"
)

const (
	// ExitCodeOK ...
	ExitCodeOK int = 0
	// ExitCodeError ...
	ExitCodeError int = 1
	// DefaultConfigFileName...
	DefaultConfigFileName string = "config.toml"
)

// CLI ...
type CLI struct {
	outStream io.Writer
	errStream io.Writer
}

// Config ...
type Config struct {
	AccessToken  string `toml:"access_token"`
	TeamName     string `toml:"team_name"`
}

// Run ...
func (c *CLI) Run(args []string) int {
	var configPath string

	app := cli.NewApp()
	app.Name = "esatopdf"
	app.Version = Version
	app.Usage = "Convert esa page url to PDF"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "config, c",
			Usage:       "Load configration from `FILE`",
			Destination: &configPath,
			Value:       defaultConfigPath(),
		},
	}
	app.Action = func(c *cli.Context) error {
		cnf, err := loadConfig(configPath)
		if err != nil {
			return err
		}

		return convert(c, cnf)
	}

	err := app.Run(args)
	if err != nil {
		fmt.Fprintln(c.errStream, err)
		return ExitCodeError
	}

	return ExitCodeOK
}

func defaultConfigPath() string {
	home, err := homedir.Dir(); if err != nil {
		panic(err)
	}
	return fmt.Sprintf("%s/.config/esatopdf/%s", home, DefaultConfigFileName)
}

func loadConfig(path string) (*Config, error) {
	c := &Config{}
	if _, err := toml.DecodeFile(path, c); err != nil {
		return nil, err
	}
	return c, nil
}

func convert(ctx *cli.Context, cnf *Config) error {
	client := esa.NewClient(cnf.AccessToken)

	assined := regexp.MustCompile("/([0-9]+)$")
	fstr := assined.FindString(os.Args[1])
	pn, _ := strconv.Atoi(fstr)
	post, err := client.Post.GetPost(cnf.TeamName, pn); if err != nil {
		return err
	}
	fmt.Printf("%s", post.BodyMd)
	return nil
}
